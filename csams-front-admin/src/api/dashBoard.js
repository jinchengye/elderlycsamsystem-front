import request from '@/utils/request'
export function articleSum() {
  return request({
    url: '/content/article/total',
    method: 'get'
  })
}
export function diseabledCount() {
  return request({
    url: '/knowledge/diseabled/total',
    method: 'get'
  })
}
export function userCount() {
  return request({
    url: '/system/user/total',
    method: 'get'
  })
}
export function elderlyCount() {
  return request({
    url: '/personal/elderly/total',
    method: 'get'
  })
}
export function viewCount7Day(url) {
  return request({
    url: url,
    method: 'get'
  })
}
export function getUserAreaList() {
  return request({
    url: '/system/dashboard/getUserAreaList',
    method: 'get'
  })
}

export function getDangerElder(query) {
  return request({
    url: '/system/dashboard/getDangerElderly',
    method: 'get',
    params: query
  })
}
