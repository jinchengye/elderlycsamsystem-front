import request from '@/utils/request'

export function listJob(query) {
  return request({
    url: '/system/schedule/list',
    method: 'get',
    params: query
  })
}

export function updateJobStatus(data) {
  return request({
    url: '/system/schedule/updateStatus',
    method: 'post',
    data: data
  })
}

export function addJob(data) {
  return request({
    url: '/system/schedule/add',
    method: 'post',
    data: data
  })
}

export function updateJob(data) {
  return request({
    url: '/system/schedule/update',
    method: 'post',
    data: data
  })
}

export function getJobById(id) {
  return request({
    url: '/system/schedule/' + id,
    method: 'get'
  })
}

export function deleteJobById(id) {
  return request({
    url: '/system/schedule/delete/' + id,
    method: 'delete'
  })
}

export function executeJob(data) {
  return request({
    url: '/system/schedule/execute',
    method: 'post',
    data: data
  })
}
