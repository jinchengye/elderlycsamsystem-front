import request from '@/utils/request'
import {praseStrEmpty} from "@/utils/sg";

export function listUnAnswerQuestion(query) {
  return request({
    url: '/knowledge/unAnswer/list',
    method: 'get',
    params: query
  })
}

export function processUnAnswerQuestion(id) {
  return request({
    url: '/knowledge/unAnswer/updateProcessStatus/' + id,
    method: 'put'
  })
}

export function listKeyword(query) {
  return request({
    url: '/knowledge/keyword/list',
    method: 'get',
    params: query
  })
}

export function listWordType() {
  return request({
    url: '/knowledge/keyword/wordType',
    method: 'get'
  })
}

export function getKeyword(id) {
  return request({
    url: '/knowledge/keyword/' + praseStrEmpty(id),
    method: 'get'
  })
}

export function updateKeyword(data) {
  return request({
    url: '/knowledge/keyword',
    method: 'put',
    data: data
  })
}

export function addKeyword(data) {
  return request({
    url: '/knowledge/keyword',
    method: 'post',
    data: data
  })
}

export function delKeyword(data) {
  return request({
    url: '/knowledge/keyword/delete',
    method: 'post',
    data: data
  })
}

export function updateCache() {
  return request({
    url: '/knowledge/keyword/updateCache',
    method: 'put'
  })
}
