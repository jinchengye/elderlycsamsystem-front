import request from '@/utils/request'
import { praseStrEmpty } from '@/utils/sg'
// 查询疾病列表
export function listDiseabled(query) {
  return request({
    url: '/knowledge/diseabled/list',
    method: 'get',
    params: query
  })
}
// 查询疾病详细
export function getDiseabled(id) {
  return request({
    url: '/knowledge/diseabled/' + praseStrEmpty(id),
    method: 'get'
  })
}

// 新增疾病
export function addDiseabled(data) {
  return request({
    url: '/knowledge/diseabled',
    method: 'post',
    data: data
  })
}
// 删除疾病
export function delDiseabled(id) {
  return request({
    url: '/knowledge/diseabled/' + id,
    method: 'delete'
  })
}

// 修改疾病
export function updateDiseabled(data) {
  return request({
    url: '/knowledge/diseabled',
    method: 'put',
    data: data
  })
}
export function search(query) {
  return request({
    url: '/knowledge/diseabled/searchNodeByName',
    method: 'get',
    params: query
  })
}
export function updateCache() {
  return request({
    url: '/knowledge/diseabled/updateCache',
    method: 'get'
  })
}
