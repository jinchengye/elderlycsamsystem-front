import request from '@/utils/request'

export function getAllTag(query) {
  return request({
    url: query.url,
    method: 'get',
    params: query.params
  })
}

export function getFullTag(url) {
  return request({
    url: url,
    method: 'get'
  })
}
