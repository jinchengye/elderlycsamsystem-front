import request from '@/utils/request'
import { praseStrEmpty } from '@/utils/sg'
export function getContact(userId) {
  return request({
    url: '/websocket/message/getContacts',
    method: 'get',
    params: { 'id': userId }
  })
}
export function getMessages(pageNumber, pageSize, userId, toUserId) {
  return request({
    url: '/websocket/messages',
    method: 'get',
    params: { 'pageNumber': pageNumber, 'pageSize': pageSize, 'userId': userId, 'toUserId': toUserId }
  })
}
export function getTotalUnread(id) {
  return request({
    url: '/websocket/message/totalUnread',
    method: 'get',
    params: { 'id': id }
  })
}
export function updateMessagesCheck(fromUserId, toUserId) {
  return request({
    url: '/websocket/message/updateMessageCheck',
    method: 'put',
    params: { 'fromUserId': fromUserId, 'toUserId': toUserId }
  })
}

export function updateMessageByMessageID(messageId) {
  return request({
    url: '/websocket/message/updateMessageByID',
    method: 'put',
    params: { 'messageId': messageId }
  })
}

export function addMessage(data) {
  return request({
    url: '/websocket/message/addMessage',
    method: 'post',
    data: data
  })
}

export function uploadFile(file, message) {
  const formData = new FormData()
  formData.append('file', file)
  formData.append('message', message)
  return request({
    url: '/websocket/message/uploadFile',
    headers: { 'Content-Type': 'multipart/form-data' },
    method: 'post',
    data: formData
  })
}

export function getUnreadContracts(id) {
  return request({
    url: '/websocket/message/unreadContract/' + id,
    method: 'get'
  })
}
export function updateReadStatus(hid) {
  return request({
    url: '/websocket/message/updateReadStatus/' + hid,
    method: 'get'
  })
}
export function listMailInfo(id) {
  return request({
    url: '/websocket/message/listMailInfo/' + id,
    method: 'get'
  })
}
