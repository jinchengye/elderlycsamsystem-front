import request from '@/utils/request'
import { praseStrEmpty } from '@/utils/sg'
import downloadService from '@/utils/downloadService'
// 查询用户列表
export function listPost() {
  return request({
    url: '/personal/post/list',
    method: 'get'
  })
}
