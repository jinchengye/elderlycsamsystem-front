import request from '@/utils/request'
import { praseStrEmpty } from '@/utils/sg'
import downloadService from '@/utils/downloadService'
// 查询用户列表
export function listDept(query) {
  return request({
    url: '/personal/dept/list',
    method: 'get',
    params: query
  })
}
// 查询用户详细
export function getDept(id) {
  return request({
    url: '/personal/dept/' + praseStrEmpty(id),
    method: 'get'
  })
}

// 新增用户
export function addDept(data) {
  return request({
    url: '/personal/dept',
    method: 'post',
    data: data
  })
}
// 删除用户
export function delDept(id) {
  return request({
    url: '/personal/dept/' + id,
    method: 'delete'
  })
}

// 修改用户
export function updateDept(data) {
  return request({
    url: '/personal/dept',
    method: 'put',
    data: data
  })
}

// 导出数据
export function exportDept() {
  return downloadService({
    url: '/personal/dept/export',
    method: 'get'
  })
}
export function uploadExcel(file) {
  const formData = new FormData()
  formData.append('file', file)
  return request({
    url: '/personal/dept/upload',
    headers: { 'Content-Type': 'multipart/form-data' },
    method: 'post',
    data: formData
  })
}
