import request from '@/utils/request'
import { praseStrEmpty } from '@/utils/sg'
import downloadService from '@/utils/downloadService'
// 查询用户列表
export function listElderly(query) {
  return request({
    url: '/personal/elderly/list',
    method: 'get',
    params: query
  })
}
// 查询用户详细
export function getElderly(id) {
  return request({
    url: '/personal/elderly/' + praseStrEmpty(id),
    method: 'get'
  })
}

// 新增用户
export function addElderly(data) {
  return request({
    url: '/personal/elderly',
    method: 'post',
    data: data
  })
}
// 删除用户
export function delElderly(id) {
  return request({
    url: '/personal/elderly/' + id,
    method: 'delete'
  })
}

// 修改用户
export function updateElderly(data) {
  return request({
    url: '/personal/elderly',
    method: 'put',
    data: data
  })
}

// 导出数据
export function exportElderly() {
  return downloadService({
    url: '/personal/elderly/export',
    method: 'get'
  })
}
export function uploadExcel(file) {
  const formData = new FormData()
  formData.append('file', file)
  return request({
    url: '/personal/elderly/upload',
    headers: { 'Content-Type': 'multipart/form-data' },
    method: 'post',
    data: formData
  })
}

export function queryUser(query) {
  return request({
    url: '/system/user/queryUser',
    method: 'get',
    params: query
  })
}
