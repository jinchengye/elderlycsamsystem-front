import request from '@/utils/request'
import downloadService from '@/utils/downloadService'

export function listHealthy(query) {
  return request({
    url: '/personal/healthy/list',
    method: 'get',
    params: query
  })
}

// 导出数据
export function exportHealthy() {
  return downloadService({
    url: '/personal/healthy/export',
    method: 'get'
  })
}
export function uploadHealthy(file) {
  const formData = new FormData()
  formData.append('file', file)
  return request({
    url: '/personal/healthy/upload',
    headers: { 'Content-Type': 'multipart/form-data' },
    method: 'post',
    data: formData
  })
}

export function getElderly(query) {
  return request({
    url: '/personal/healthy/getElderly',
    method: 'get',
    params: query
  })
}

export function updateHealthy(data) {
  return request({
    url: '/personal/healthy/update',
    method: 'post',
    data: data
  })
}

// 新增用户
export function addHealthy(data) {
  return request({
    url: '/personal/healthy/add',
    method: 'post',
    data: data
  })
}
// 删除用户
export function delHealthy(id) {
  return request({
    url: '/personal/healthy/' + id,
    method: 'delete'
  })
}

export function getHealthy(id) {
  return request({
    url: '/personal/healthy/' + id,
    method: 'get'
  })
}

