export default [
  {
    label: "表情",
    children: [
      {
        name: "1f600",
        title: "微笑",
        src: "static/emot/image/weixiao.gif"
      },
      {
        name: "1f62c",
        title: "嘻嘻",
        src: "static/emot/image/xixi.gif"
      },
      {
        name: "1f601",
        title: "哈哈",
        src: "static/emot/image/haha.gif"
      },
      {
        name: "1f602",
        title: "可爱",
        src: "static/emot/image/keai.gif"
      },
      {
        name: "1f923",
        title: "可怜",
        src: "static/emot/image/kelian.gif"
      },
      {
        name: "1f973",
        title: "挖鼻",
        src: "static/emot/image/wabi.gif"
      },
      {
        name: "1f603",
        title: "吃惊",
        src: "static/emot/image/chijing.gif"
      },
      {
        name: "1f604",
        title: "害羞",
        src: "static/emot/image/haixiu.gif"
      },
      {
        name: "1f605",
        title: "挤眼",
        src: "static/emot/image/jiyan.gif"
      },
      {
        name: "1f606",
        title: "闭嘴",
        src: "static/emot/image/bizui.gif"
      },
      {
        name: "1f607",
        title: "鄙视",
        src: "static/emot/image/bishi.gif"
      },
      {
        name: "1f609",
        title: "爱你",
        src: "static/emot/image/aini.gif"
      },
      {
        name: "1f60a",
        title: "泪",
        src: "static/emot/image/lei.gif"
      },
      {
        name: "1263a",
        title: "生病",
        src: "static/emot/image/shengbing.gif"
      },
      {
        name: "1f60b",
        title: "太开心",
        src: "static/emot/image/taikaixin.gif"
      },
      {
        name: "1f60c",
        title: "白眼",
        src: "static/emot/image/baiyan.gif"
      },
      {
        name: "1f60d",
        title: "右哼哼",
        src: "static/emot/image/youhengheng.gif"
      },
      {
        name: "1f970",
        title: "左哼哼",
        src: "static/emot/image/zuohengheng.gif"
      },
      {
        name: "1f618",
        title: "嘘",
        src: "static/emot/image/xu.gif"
      },
      {
        name: "1f617",
        title: "衰",
        src: "static/emot/image/shuai.gif"
      },
      {
        name: "1f619",
        title: "吐",
        src: "static/emot/image/tu.gif"
      },
      {
        name: "1f61a",
        title: "哈欠",
        src: "static/emot/image/haqian.gif"
      },
      {
        name: "1f61c",
        title: "抱抱",
        src: "static/emot/image/baobao.gif"
      },
      {
        name: "1f92a",
        title: "怒",
        src: "static/emot/image/nu.gif"
      },
      {
        name: "1f928",
        title: "疑问",
        src: "static/emot/image/yiwen.gif"
      },
      {
        name: "1f9d0",
        title: "馋嘴",
        src: "static/emot/image/chanzui.gif"
      },
      {
        name: "1f61d",
        title: "拜拜",
        src: "static/emot/image/baibai.gif"
      },
      {
        name: "1f61b",
        title: "思考",
        src: "static/emot/image/sikao.gif"
      },
      {
        name: "1f911",
        title: "汗",
        src: "static/emot/image/han.gif"
      },
      {
        name: "1f913",
        title: "困",
        src: "static/emot/image/kun.gif"
      },
      {
        name: "1f60e",
        title: "睡",
        src: "static/emot/image/shui.gif"
      },
      {
        name: "1f929",
        title: "钱",
        src: "static/emot/image/qian.gif"
      },
      {
        name: "1f921",
        title: "失望",
        src: "static/emot/image/shiwang.gif"
      },
      {
        name: "1f920",
        title: "酷",
        src: "static/emot/image/ku.gif"
      },
      {
        name: "1f917",
        title: "色",
        src: "static/emot/image/se.gif"
      },
      {
        name: "1f60f",
        title: "哼",
        src: "static/emot/image/heng.gif"
      },
      {
        name: "1f610",
        title: "晕",
        src: "static/emot/image/yun.gif"
      },
      {
        name: "1f611",
        title: "悲伤",
        src: "static/emot/image/beishang.gif"
      },
      {
        name: "1f612",
        title: "抓狂",
        src: "static/emot/image/zhuakuang.gif"
      },
      {
        name: "1f914",
        title: "阴险",
        src: "static/emot/image/yinxian.gif"
      },
      {
        name: "1f925",
        title: "怒骂",
        src: "static/emot/image/numa.gif"
      },
      {
        name: "1f92d",
        title: "互粉",
        src: "static/emot/image/hufen.gif"
      },
      {
        name: "1f92b",
        title: "书呆子",
        src: "static/emot/image/shudaizi.gif"
      },
      {
        name: "1f92c",
        title: "愤怒",
        src: "static/emot/image/fennu.gif"
      },
      {
        name: "1f92f",
        title: "感冒",
        src: "static/emot/image/ganmao.gif"
      },
      {
        name: "1f61e",
        title: "伤心",
        src: "static/emot/image/shangxin.gif"
      },
      {
        name: "1f61f",
        title: "猪",
        src: "static/emot/image/zhu.gif"
      },
      {
        name: "1f620",
        title: "熊猫",
        src: "static/emot/image/xiongmao.gif"
      },
      {
        name: "1f621",
        title: "兔子",
        src: "static/emot/image/tuzi.gif"
      },
      {
        name: "1f622",
        title: "喔克",
        src: "static/emot/image/ok.gif"
      },
      {
        name: "1f623",
        title: "鼓掌",
        src: "static/emot/image/guzhang.gif"
      },
      {
        name: "1f627",
        title: "耶",
        src: "static/emot/image/ye.gif"
      },
      {
        name: "1f628",
        title: "棒棒",
        src: "static/emot/image/good.gif"
      },
      {
        name: "1f629",
        title: "不",
        src: "static/emot/image/no.gif"
      },
      {
        name: "1f630",
        title: "赞",
        src: "static/emot/image/zan.gif"
      },
      {
        name: "1f631",
        title: "来",
        src: "static/emot/image/lai.gif"
      },
      {
        name: "1f632",
        title: "弱",
        src: "static/emot/image/ruo.gif"
      },
      {
        name: "1f633",
        title: "心",
        src: "static/emot/image/xin.gif"
      },
      {
        name: "1f634",
        title: "草泥马",
        src: "static/emot/image/caonima.gif"
      },
      {
        name: "1f635",
        title: "神马",
        src: "static/emot/image/shenma.gif"
      },
      {
        name: "1f636",
        title: "囧",
        src: "static/emot/image/jiong.gif"
      },
      {
        name: "1f637",
        title: "浮云",
        src: "static/emot/image/fuyun.gif"
      },
      {
        name: "1f638",
        title: "给力",
        src: "static/emot/image/geili.gif"
      },
      {
        name: "1f639",
        title: "围观",
        src: "static/emot/image/weiguan.gif"
      },
      {
        name: "1f640",
        title: "威武",
        src: "static/emot/image/weiwu.gif"
      },
      {
        name: "1f641",
        title: "话筒",
        src: "static/emot/image/huatong.gif"
      },
      {
        name: "1f642",
        title: "蜡烛",
        src: "static/emot/image/lazhu.gif"
      },
      {
        name: "1f643",
        title: "蛋糕",
        src: "static/emot/image/dangao.gif"
      },
      {
        name: "1f644",
        title: "发红包",
        src: "static/emot/image/fahongbao.gif"
      },
      {
        name: "1f645",
        title: "偷笑",
        src: "static/emot/image/touxiao.gif"
      },
      {
        name: "1f646",
        title: "亲亲",
        src: "static/emot/image/qinqin.gif"
      },
      {
        name: "1f647",
        title: "黑线",
        src: "static/emot/image/heixian.gif"
      },
    ]
  },
  {
    label: "收藏",
    children: [
    ]
  }
];
