import { parseMessage } from '@/utils/sg'

const webSocket = {
  state: {
    webSocketMsg: '',
    currentContractId: null,
    unread: 0,
    unreadContract: [],
    mailUnread: 0,
    mailContent: []
  },
  mutations: {
    SET_WS_MSG: (state, msg) => {
      state.webSocketMsg = msg
    },
    SET_CURRENT_CONTRACT: (state, id) => {
      state.currentContractId = id
    },
    SET_MAIL_CONTENT: (state, msg) => {
      state.mailContent.unshift(msg)
    },
    SET_MAIL_CONTENT_LIST: (state, list) => {
      state.mailContent = list
    },
    RESET_MAIL_CONTENT: (state) => {
      state.mailContent = []
    },
    REMOVE_MAIL_CONTENT: (state, mailContent) => {
      state.mailContent = state.mailContent.map(item => {
        if(item.hid === mailContent.hid) {
          item.readStatus = '1'
        }
        return item
      })
    },
    ADD_MAIL_UNREAD: (state) => {
      state.mailUnread += 1
    },
    SUB_MAIL_UNREAD: (state, sub) => {
      state.mailUnread -= sub
      if (state.mailUnread < 0) { state.mailUnread = 0 }
    },
    SET_MAIL_UNREAD: (state, num) => {
      state.mailUnread = num
    },
    ADD_UNREAD: (state) => {
      state.unread += 1
    },
    SUB_UNREAD: (state, sub) => {
      state.unread -= sub
      if (state.unread < 0) { state.unread = 0 }
    },
    SET_UNREAD: (state, num) => {
      state.unread = num
    },
    ADD_UNREAD_CONTRACT: (state, contract) => {
      let is_exist = false
      state.unreadContract.map(item => {
        if (item.id === contract.id) {
          is_exist = true
          item.unread += 1
          item.lastContent = parseMessage(contract.lastContent, contract.type)
          item.lastSendTime = contract.lastSendTime
          return item
        }
      })
      if (!is_exist) {
        state.unreadContract.push(contract)
      }
    },
    SUB_UNREAD_CONTRACT: (state, contract) => {
      state.unreadContract = state.unreadContract.filter(item => item.id !== contract.id)
    },
    SET_UNREAD_CONTRACT: (state, contractList) => {
      contractList.map(item => {
        item.lastContent = parseMessage(item.lastContent, item.type)
        return item
      })
      state.unreadContract = contractList
    }
  },
  actions: {
    setMailContent({ commit }, data) {
      return new Promise(resolve => {
        commit('SET_MAIL_CONTENT', data)
        commit('ADD_MAIL_UNREAD')
        resolve()
      })
    },
    setMailContentList({ commit }, list) {
      return new Promise(resolve => {
        commit('SET_MAIL_CONTENT_LIST', list)
        resolve()
      })
    },
    resetMailContent({ commit }) {
      return new Promise(resolve => {
        commit('RESET_MAIL_CONTENT')
        commit('SET_MAIL_UNREAD', 0)
        resolve()
      })
    },
    removeMailContent({ commit }, mailContent) {
      return new Promise(resolve => {
        commit('REMOVE_MAIL_CONTENT', mailContent)
        commit('SUB_MAIL_UNREAD', 1)
        resolve()
      })
    },
    setWebsocketMsg({ commit }, data) {
      return new Promise(resolve => {
        commit('SET_WS_MSG', data)
        resolve()
      })
    },
    setCurrentContractId({ commit }, id) {
      return new Promise(resolve => {
        commit('SET_CURRENT_CONTRACT', id)
        resolve()
      })
    },
    addMailUnread({ commit }) {
      return new Promise(resolve => {
        commit('ADD_MAIL_UNREAD')
        resolve()
      })
    },
    subMailUnread({ commit }, sub) {
      return new Promise(resolve => {
        commit('SUB_MAIL_UNREAD', sub)
        resolve()
      })
    },
    setMailUnread({ commit }, num) {
      return new Promise(resolve => {
        commit('SET_MAIL_UNREAD', num)
        resolve()
      })
    },
    addUnread({ commit }) {
      return new Promise(resolve => {
        commit('ADD_UNREAD')
        resolve()
      })
    },
    subUnread({ commit }, sub) {
      return new Promise(resolve => {
        commit('SUB_UNREAD', sub)
        resolve()
      })
    },
    setUnread({ commit }, num) {
      return new Promise(resolve => {
        commit('SET_UNREAD', num)
        resolve()
      })
    },
    addUnreadContract({ commit }, contract) {
      return new Promise(resolve => {
        commit('ADD_UNREAD_CONTRACT', contract)
        resolve()
      })
    },
    subUnreadContract({ commit }, contract) {
      return new Promise(resolve => {
        commit('SUB_UNREAD_CONTRACT', contract)
        resolve()
      })
    },
    setUnreadContract({ commit }, contractList) {
      return new Promise(resolve => {
        commit('SET_UNREAD_CONTRACT', contractList)
        resolve()
      })
    }
  }
}

export default webSocket

