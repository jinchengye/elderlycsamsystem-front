import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import user from './modules/user'
import permission from './modules/permission'
import settings from './modules/settings'
import webSocket from './modules/webSocket'

Vue.use(Vuex)
export const state = {
  baseURL: 'http://localhost:8082/'
}
const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user,
    permission,
    webSocket
  },
  getters,
  state
})

export default store
