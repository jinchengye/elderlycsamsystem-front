import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import * as echarts from 'echarts'
import directive from './directive' // directive
import '@/styles/index.scss' // global css
import h from 'vue'
import App from './App'
import store from './store'
import router from './router'
import plugins from './plugins' // plugins
import './assets/icons' // icon
import '@/permission' // permission control
import './assets/iconfont/iconfont.css'
import { parseTime, resetForm, addDateRange, selectDictLabel, selectDictLabels, handleTree, parseCron } from '@/utils/sg'
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
import LemonIMUI from 'lemon-imui'
import 'lemon-imui/dist/index.css'
import WS from './utils/webSocket'
import Clipboard from 'v-clipboard'

import VueCron from 'vue-cron'
Vue.use(VueCron)// 使用方式：<vueCron></vueCron>
Vue.use(LemonIMUI)
Vue.use(Clipboard)
Vue.use(directive)
Vue.use(ElementUI)
Vue.use(plugins)
Vue.use(mavonEditor)
// 全局方法挂载
Vue.prototype.$ws = WS
Vue.prototype.parseCron = parseCron
Vue.prototype.parseTime = parseTime
Vue.prototype.resetForm = resetForm
Vue.prototype.addDateRange = addDateRange
Vue.prototype.selectDictLabel = selectDictLabel
Vue.prototype.selectDictLabels = selectDictLabels
Vue.prototype.handleTree = handleTree
Vue.prototype.$echarts = echarts
Vue.config.productionTip = false
Vue.filter('dataFormat3', function(originVal) {
  const dt = new Date(originVal)
  const usedTime = new Date() - dt
  const days = Math.floor(usedTime / (24 * 3600 * 1000)) // 计算出天数
  const leavel = usedTime % (24 * 3600 * 1000) // 计算天数后剩余的时间
  const hours = Math.floor(leavel / (3600 * 1000)) // 计算剩余的小时数
  const leavel2 = leavel % (3600 * 1000) // 计算剩余小时后剩余的毫秒数
  const minutes = Math.floor(leavel2 / (60 * 1000)) // 计算剩余的分钟数
  if (days > 0) return days + '天前'
  if (hours > 0) return hours + '小时前'
  return minutes + '分前'
})
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
