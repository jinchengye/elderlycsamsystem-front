# 基于SpringBoot + Vue的老年人社区服务与管理系统

#### 介绍
老年人社区服务与管理平台前端

#### 后端地址
https://gitee.com/jinchengye/elderlycsamsystem

#### 相关技术概述
Vue、Element-UI、Lemon-IMUI、SpringBoot、Mybatis-Plus、 MySQL、Redis、Neo4j
本系统所用到的相关的框架以及技术，这里只介绍了主要的几种，实际上还包含了WebSocket 、 SpringSecurity、阿里巴巴的EasyExcel、七牛云、以及自己搭建的邮件服务器、前端的各种插件等等，奈何时间有限就这里就不过多赘述了

#### 运行截图
运行截图在后端项目的Markdown文档上





