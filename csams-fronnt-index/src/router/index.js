import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

export default new Router({
	routes: [{
			path: '/',
			component: resolve => require(['../pages/Login.vue'], resolve),
			meta: {
				auth: true
			},
			name: 'Login'
		}, //首页
		{
			path: '/Home',
			component: resolve => require(['../pages/Home.vue'], resolve),
			meta: {
				auth: true
			},
			name: 'Home',
      children: [
        {
          path:'/DashBoard',
          component: resolve => require(['../pages/DashBoard.vue'], resolve),
          meta: {
            auth: true
          },
          name: 'DashBoard'
        },
        {
          path: '/UserInfo',
          component: resolve => require(['../pages/UserInfo.vue'], resolve),
          meta: {
            auth: true
          },
          name: 'UserInfo'
        },
        {
          path: '/announcement',
          component: resolve => require(['../pages/Announcement.vue'], resolve),
          meta: {
            auth: true
          },
          name: 'Announcement'
        },
        {
          path: '/announcementDetail',
          component: resolve => require(['../pages/article/AnnouncementDetail.vue'], resolve),
          meta: {
            auth: true
          },
          name: 'AnnouncementDetail'
        },
        {
          path: '/ArticleDetail',
          component: resolve => require(['../pages/article/ArticleDetail.vue'], resolve),
          meta: {
            auth: true
          },
          name: 'ArticleDetail'
        },
        {
          path: '/Dynamics',
          component: resolve => require(['../pages/Dynamics.vue'], resolve),
          meta: {
            auth: true
          },
          name: 'Dynamics'
        }
      ]
		}, //首页

		{
			path: '/Login',
			component: resolve => require(['../pages/Login.vue'], resolve),
			meta: {
				auth: false
			},
			name: 'Login'
		}, //注册登录
	]
})
