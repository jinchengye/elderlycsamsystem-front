export function parseArray(array, key) {
  const tmp = []
  array.forEach(a => {
    tmp.push({ id: a[key].id, name: a[key].name })
  })
  return tmp
}
export function cachedOptions(selected, item) {
  item.forEach(a => {
    selected.push({
      currentLabel: a.name,
      currentValue: {
        id: a.id,
        name: a.name
      },
      label: a.name,
      value: {
        id: a.id,
        name: a.name
      }
    })
  })
}
export function coverStringToObj(array) {
  return array.map(a => {
    if (typeof a === 'string') {
      return { id: null, name: a }
    } else {
      return a
    }
  })
}
// 日期格式化
export function parseTime(time, pattern) {
  if (arguments.length === 0 || !time) {
    return null
  }
  const format = pattern || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if ((typeof time === 'string') && (/^[0-9]+$/.test(time))) {
      time = parseInt(time)
    } else if (typeof time === 'string') {
      time = time.replace(new RegExp(/-/gm), '/').replace('T', ' ').replace(new RegExp(/\.[\d]{3}/gm), '')
    }
    if ((typeof time === 'number') && (time.toString().length === 10)) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') { return ['日', '一', '二', '三', '四', '五', '六'][value] }
    if (result.length > 0 && value < 10) {
      value = '0' + value
    }
    return value || 0
  })
  return time_str
}

// 表单重置
export function resetForm(refName) {
  if (this.$refs[refName]) {
    this.$refs[refName].resetFields()
  }
}

// 添加日期范围
export function addDateRange(params, dateRange, propName) {
  const search = params
  search.params = typeof (search.params) === 'object' && search.params !== null && !Array.isArray(search.params) ? search.params : {}
  dateRange = Array.isArray(dateRange) ? dateRange : []
  if (typeof (propName) === 'undefined') {
    search.params['beginTime'] = dateRange[0]
    search.params['endTime'] = dateRange[1]
  } else {
    search.params['begin' + propName] = dateRange[0]
    search.params['end' + propName] = dateRange[1]
  }
  return search
}

// 回显数据字典
export function selectDictLabel(datas, value) {
  var actions = []
  Object.keys(datas).some((key) => {
    if (datas[key].value === ('' + value)) {
      actions.push(datas[key].label)
      return true
    }
  })
  return actions.join('')
}

// 回显数据字典（字符串数组）
export function selectDictLabels(datas, value, separator) {
  var actions = []
  var currentSeparator = undefined === separator ? ',' : separator
  var temp = value.split(currentSeparator)
  Object.keys(value.split(currentSeparator)).some((val) => {
    Object.keys(datas).some((key) => {
      if (datas[key].value === ('' + temp[val])) {
        actions.push(datas[key].label + currentSeparator)
      }
    })
  })
  return actions.join('').substring(0, actions.join('').length - 1)
}

// 字符串格式化(%s )
export function sprintf(str) {
  var args = arguments; var flag = true; var i = 1
  str = str.replace(/%s/g, function() {
    var arg = args[i++]
    if (typeof arg === 'undefined') {
      flag = false
      return ''
    }
    return arg
  })
  return flag ? str : ''
}

// 转换字符串，undefined,null等转化为""
export function praseStrEmpty(str) {
  if (!str || str === 'undefined' || str === 'null') {
    return ''
  }
  return str
}

// 数据合并
export function mergeRecursive(source, target) {
  for (var p in target) {
    try {
      if (target[p].constructor == Object) {
        source[p] = mergeRecursive(source[p], target[p])
      } else {
        source[p] = target[p]
      }
    } catch (e) {
      source[p] = target[p]
    }
  }
  return source
}

/**
 * 构造树型结构数据
 * @param {*} data 数据源
 * @param {*} id id字段 默认 'id'
 * @param {*} parentId 父节点字段 默认 'parentId'
 * @param {*} children 孩子节点字段 默认 'children'
 */
export function handleTree(data, id, parentId, children) {
  const config = {
    id: id || 'id',
    parentId: parentId || 'parentId',
    childrenList: children || 'children'
  }

  var childrenListMap = {}
  var nodeIds = {}
  var tree = []

  for (const d of data) {
    const parentId = d[config.parentId]
    if (childrenListMap[parentId] == null) {
      childrenListMap[parentId] = []
    }
    nodeIds[d[config.id]] = d
    childrenListMap[parentId].push(d)
  }

  for (const d of data) {
    const parentId = d[config.parentId]
    if (nodeIds[parentId] == null) {
      tree.push(d)
    }
  }

  for (const t of tree) {
    adaptToChildrenList(t)
  }

  function adaptToChildrenList(o) {
    if (childrenListMap[o[config.id]] !== null) {
      o[config.childrenList] = childrenListMap[o[config.id]]
    }
    if (o[config.childrenList]) {
      for (const c of o[config.childrenList]) {
        adaptToChildrenList(c)
      }
    }
  }
  return tree
}

export function parseMessage(str, type) {
  if (type === 'text') {
    return eclipse(emojiNameToImage(clearHtml(str)))
  } else if (type === 'file') {
    return '发送了一个[文件]'
  } else if (type === 'image') {
    return '发送了一张[图片]'
  }
}
export function eclipse(str) {
  if (str === '') {
    return ''
  }
  if (str.length > 30) {
    return str.slice(0, 30) + '...'
  }
  return str
}
export function emojiNameToImage(str) {
  return str.replace(/\[!(\w+)\]/gi, function(str, match) {
    return '[表情]'
  })
}
export function clearHtml(str) {
  return str.replace(/<.*?>/gi, '')
} // 清除字符串内的所有HTML标签，除了IMG
//YYYY-MM-dd
export function formatDateNotTime(dateTime) {
  var date = new Date(dateTime);
  var timeStr = date.getFullYear() + '-';
  if(date.getMonth() < 9) {
    //月份从0开始的
    timeStr += '0';
  }
  timeStr += date.getMonth() + 1 + '-';
  if(date.getDate() < 9) {
    timeStr += '0';
  }
  timeStr += date.getDate()
  return timeStr
}
//JavaScript函数：
export function getDateDiff(value) {
  const dateTimeStamp = getDateTimeStamp(value)
  const minute = 1000 * 60;
  const hour = minute * 60;
  const day = hour * 24;
  const halfamonth = day * 15;
  const month = day * 30;

  const now = new Date().getTime();
  const diffValue = now - dateTimeStamp;
  if (diffValue < 0) {
    //若日期不符则弹出窗口告之
    //alert("结束日期不能小于开始日期！");
  }
  const date = new Date(dateTimeStamp)
  const monthC = diffValue / month;
  const weekC = diffValue / (7 * day);
  const dayC = diffValue / day;
  const hourC = diffValue / hour;
  const minC = diffValue / minute;
  let  result = ""
  if (monthC >= 1) {
    result = parseInt(monthC) + "个月前";
  } else if (weekC >= 1 || parseInt(dayC) > 1) {
    result = formatDateNotTime(date)
  } else if (parseInt(dayC) === 1) {
    result = "昨天" + (date.getHours() < 10 ? ('0' + date.getHours()) : date.getHours()) + ':' + (date.getMinutes() < 10 ? ('0' + date.getMinutes()) : date.getMinutes())
  } else if (hourC >= 1) {
    result = parseInt(hourC) + "个小时前";
  } else if (minC >= 1) {
    result = parseInt(minC) + "分钟前";
  } else
    result = "刚刚发表";
  return result;
}
//js函数代码：字符串转换为时间戳
export function getDateTimeStamp(dateStr){
  return Date.parse(dateStr.replace(/-/gi,"/"));
}
