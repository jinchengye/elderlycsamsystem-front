/*
  邮箱校验
 */
export function isEmail (s) {
    return /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/.test(s)
  }
/* 
  校验密码：只能输入6-12个字母、数字、下划线 
*/
export function isPasswd(s) {  
    var patrn=/^(\w){6,12}$/;  
    if (!patrn.exec(s)) return false
    return true
 }  