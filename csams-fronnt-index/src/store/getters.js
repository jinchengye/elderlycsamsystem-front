const getters = {
  webSocketMsg: state => state.webSocket.webSocketMsg,
  currentContractId: state => state.webSocket.currentContractId,
  unread: state => state.webSocket.unread,
  unreadContract: state => state.webSocket.unreadContract,
  mailUnread: state => state.webSocket.mailUnread,
  mailContent: state => state.webSocket.mailContent
}
export default getters
