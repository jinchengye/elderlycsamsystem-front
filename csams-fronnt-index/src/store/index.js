import Vue from 'vue'
import Vuex from 'vuex'
import webSocket from './modules/webSocket'
import getters from './getters'

Vue.use(Vuex)

/** 状态定义 */
export const state = {
  loading: false,
  themeObj: 0,//主题
  keywords:'',//关键词
  errorImg: 'this.onerror=null;this.src="' + require('../../static/img/tou.jpg') + '"',
  baseURL:'http://localhost:8081/',
  webSocketURL:'localhost:8081'
}

export default new Vuex.Store({
  modules: {
    webSocket
  },
    getters,
    state,
})
