// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './assets/iconfont/iconfont.css'
import './assets/css/style.less'
import store from './store'
import modal from './utils/modal'
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
import 'mavon-editor/src/lib/css/markdown.css'
import WS from './utils/webSocket'
import Clipboard from 'v-clipboard'
import loadingDirectiive from './utils/loading'
Vue.directive('myLoading', loadingDirectiive)
Vue.use(Clipboard)
import LemonIMUI from 'lemon-imui';
import 'lemon-imui/dist/index.css';
Vue.use(LemonIMUI);
import { formatDateNotTime , getDateTimeStamp } from "./utils/sg"
// use
Vue.use(mavonEditor)
Vue.config.productionTip = false
Vue.use(ElementUI)
// 模态框对象
Vue.prototype.$ws = WS
Vue.prototype.$modal = modal
Vue.prototype.formatDateNotTime = formatDateNotTime
Vue.prototype.getDateTimeStamp = getDateTimeStamp

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  store
})
