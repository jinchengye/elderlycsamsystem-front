import request from '@/utils/request'

export function getRegisterCode(query) {
  return request({
    url: '/code/getRegisterCode',
    method: 'get',
    params: query
  })
}
export function getForgiveCode(query) {
  return request({
    url: '/code/getForgiveCode',
    method: 'get',
    params: query
  })
}
