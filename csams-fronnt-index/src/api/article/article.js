import request from '@/utils/request'
export function getAnnouncement(query) {
  return request({
    url: '/article/announcement',
    method: 'get',
    params: query
  })
}

export function getAnnouncementDetail(id) {
  return request({
    url: '/article/announcementDetail/' + id,
    method: 'get'
  })
}


export function getArticle(id) {
  return request({
    url: '/article/getArticle/' + id,
    method: 'get'
  })
}
export function getDashboardHotArticle() {
  return request({
    url: '/article/dashboardHotArticle',
    method: 'get'
  })
}

export function getRecommendArticle() {
  return request({
    url: '/article/recommendArticle',
    method: 'get'
  })
}

export function getArticleList(query) {
  return request({
    url: '/article/articleList',
    method: 'get',
    params: query
  })
}

export function getSearchArticle(query) {
  return request({
    url: '/article/getSearchArticle',
    method: 'get',
    params: query
  })
}
