import request from '@/utils/request'

export function getRootComment(query) {
  return request({
    url: '/comment/rootList',
    method: 'get',
    params: query
  })
}

export function addRootComment(data) {
  return request({
    url: '/comment/addRootComment',
    method: 'post',
    data: data
  })
}

export function addComment(data) {
  return request({
    url: '/comment/addComment',
    method: 'post',
    data: data
  })
}

export function getMoreChildrenComment(query) {
  return request({
    url: '/comment/getRootCommentMore',
    method: 'get',
    params: query
  })
}

export function deleteComment(id) {
  return request({
    url: '/comment/deleteComment/' + id,
    method: 'delete'
  })
}
